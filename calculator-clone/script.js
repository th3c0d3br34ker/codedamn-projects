const keypad = document.querySelector('.keypad');
const screen = document.querySelector('.screen');
const errorText = document.querySelector('.error');

// Add Buttons
for (let i = 0; i < 18; i++) {
  const btn = document.createElement('button');
  btn.setAttribute('id', `btn${i}`);
  keypad.appendChild(btn);
}

const buttons = document.querySelectorAll('.keypad button');

const buttonTexts = [
  "AC", "DEL", "/",
  "1", "2", "3", "*",
  "4", "5", "6", "+",
  "7", "8", "9", "-",
  ".", "0", "="];

ops = ['/', '*', '+', '-', '.'];
nums = ['1', '2', '3', '4', '5', '6', '6', '7', '8', '9', '0'];

buttons.forEach((el, i) => {
  el.addEventListener('click', func)
  el.innerHTML = buttonTexts[i];
});

function func(e) {
  errorText.innerHTML = '';
  if (screen.value === '0') {
    screen.value = '';
  }
  const latestKey = e.target.innerText;
  let lastDigit = '';
  if (ops.includes(latestKey) || nums.includes(latestKey)) {
    let display = screen.value;
    screen.value = display + latestKey;
    lastDigit = display.substring(display.length - 1);
  }
  if (latestKey === 'AC') {
    screen.value = '0';
  } else if (latestKey === 'DEL') {
    let temp = screen.value;
    if (('' + temp).length <= 1) {
      screen.value = '0';
    } else {
      screen.value = temp.slice(0, temp.length - 1);
    }
  } else if (latestKey === '=') {
    if (ops.includes(lastDigit)) {
      let temp = screen.value;
      let newScreen = temp.slice(temp.length - 1);
      screen.value = eval(newScreen);
    }
    else {
      try {
        screen.value = eval(screen.value);
      } catch (error) {
        errorText.innerHTML = 'Error: Invalid Input';
      }
    }
  } else if (ops.includes(latestKey)) {
    if (ops.includes(lastDigit)) {
      let temp = screen.value;
      let newScreen = temp.slice(0, temp.length - 2);
      newScreen = newScreen + latestKey;
      screen.value = newScreen;
    }
  }
}

const sanitizeInput = (input) => {
  if (buttonTexts.includes(input)) {
    temp = input.toLowerCase();
  }
}
