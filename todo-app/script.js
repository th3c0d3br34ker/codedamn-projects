const form = document.querySelector('#form');
const taskInput = document.querySelector('#task-input');
const todoList = document.querySelector('#tasks');

let state = {
  tasks: [],
};

// Dynamic HTML template for new tasks
const template = task => `<div 
  id="${task.id}"
  onclick="markComplete(${task.id})"
  class="tasks">
  <p class="${task.complete ? 'task__done' : null}">${task.task}</p>
  ${removeButton(task)}
</div>
`;

// Create and add remove button
const removeButton = task => {
  if (task.complete) {
    return `
<input 
  type="button" 
  value="delete" 
  class="delete-button" 
  onclick="removeTask(${task.id})" 
/>`;
  }
  return '';
};

// Render the template to the DOM
const render = (htmlString, el) => {
  el.innerHTML += htmlString;
};


// Mark task as complete
const markComplete = (id) => {
  const i = state.tasks.findIndex((item) => item.id === id);
  if (i !== -1) {
    state.tasks[i].complete = !state.tasks[i].complete;
    todoList.innerHTML = '';
    state.tasks.map(el => render(template(el), todoList));
  }
};

// Remove task
const removeTask = (id) => {
  const index = state.tasks.findIndex((item) => item.id === id);
  state.tasks.splice(index, 1);
  todoList.innerHTML = '';
  state.tasks.map(el => render(template(el), todoList));
};

// Submit form
form.addEventListener('submit', (e) => {
  e.preventDefault()
  const task = {
    id: Date.now(),
    task: taskInput.value,
    complete: false,
  };
  state.tasks = [...state.tasks, task];
  let temp_template = template(task);
  render(template(state.tasks[state.tasks.length - 1]), todoList);
  taskInput.value = '';
});


const clearInput = () => {
  taskInput.value = '';
}
